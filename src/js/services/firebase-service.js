labs.factory('firebaseService', ['$firebaseObject', '$firebaseAuth', function($firebaseObject, $firebaseAuth) {

	function getAuth () {
		var firebaseRef = new Firebase('thespacelabs.firebaseio.com');
		return $firebaseAuth(firebaseRef);
	}

	function getUser (uid) {
		var firebaseRef = new Firebase('thespacelabs.firebaseio.com/user').child(uid);
		return $firebaseObject(firebaseRef);
	}

	function setFacebookUser (uid, name, picture) {
		var obj = getUser(uid);

		/* Check if facebook user has been previously registered */
		if (obj.picture === 'undefined') {
			obj.name = name;
			obj.picture = picture;
			obj.pitch = {};
			obj.pitch.description = '';
			obj.pitch.features = '';
			obj.pitch.name = '';
			obj.$save();
		}
	} 
	
	// public api
	return {
		getAuth: getAuth,
		getUser: getUser,
		setFacebookUser: setFacebookUser
	};
}]);