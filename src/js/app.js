var labs = angular.module('labs', ['ngRoute', 'ngCookies', 'firebase', 'ngS3upload', 'ui.bootstrap']);

labs.config(['$routeProvider',
	function($routeProvider) {
		$routeProvider.
			when('/', {
				redirectTo: '/login'
      		}).
			when('/login', {
				templateUrl: 'js/views/login.html',
        		controller: 'loginCtrl'
      		}).
      		when('/user', {
				templateUrl: 'js/views/user.html',
        		controller: 'userCtrl'
      		}).
      		otherwise({
        		redirectTo: '/login'
      		});
	}
]);