labs.controller('userCtrl', ['$scope', '$location', '$rootScope', 'firebaseService', '$cookieStore', function($scope, $location, $rootScope, firebaseService, $cookieStore) {

	if ($cookieStore.get('user') === 'undefined' || $cookieStore.get('isUserLoggedIn') === false) {
		$location.path('/login');
	}

	$scope.activeTab = 0;
	$scope.user = firebaseService.getUser($cookieStore.get('user'));

	$scope.gotoTab = function (tabIndex) {
		$scope.activeTab = tabIndex;
		document.body.scrollTop = 0;
	};

	$scope.logoutUser = function () {
		$cookieStore.remove('user');
		$cookieStore.remove('isUserLoggedIn');
		$cookieStore.remove('isSocialLogin');

		$location.path('/');
	};

	$rootScope.$on('user:change-tab', function (evt, tabIndex) {
		$scope.gotoTab(tabIndex);
	});


}]);