labs.controller('loginCtrl', ['$scope', '$location', '$rootScope', 'firebaseService', '$cookieStore', function($scope, $location, $rootScope, firebaseService, $cookieStore) {

	/* 
	Firebase object init*/
	$scope.authObj = firebaseService.getAuth();

	$scope.errorMessage = '';
	$scope.isSpinnerVisible = false;

	function checkCookies () {
		if ($cookieStore.get('user') && $cookieStore.get('isUserLoggedIn')) {
			$location.path('/user');
		}
	}

	checkCookies();

	$scope.clearErrors = function () {
		$scope.errorMessage = '';
	};
	
	$scope.validateUser = function () {
		$scope.isSpinnerVisible = true;
		
		$scope.authObj.$authWithPassword({
			email: $scope.email,
			password: $scope.password
		}).then(function(authData) {
			$cookieStore.put('user', authData.auth.uid);
			$cookieStore.put('isUserLoggedIn', true);
			$cookieStore.put('isSocialLogin', false);
			$location.path('/user');
		}).catch(function(error) {
			$scope.isSpinnerVisible = false;
			$scope.errorMessage = error.message;
		});
	};

	$scope.facebookLogin = function () {
		$scope.isSpinnerVisible = true;

		$scope.authObj.$authWithOAuthPopup('facebook').then(function(authData) {
			$cookieStore.put('user', authData.auth.uid);
			$cookieStore.put('isUserLoggedIn', true);
			$cookieStore.put('isSocialLogin', true);

			firebaseService.setFacebookUser(authData.auth.uid, authData.facebook.displayName, authData.facebook.cachedUserProfile.picture.data.url);

			$location.path('/user');
		}).catch(function(error) {
			$scope.isSpinnerVisible = false;
			$scope.errorMessage = error.message;
		});
	};

}]);