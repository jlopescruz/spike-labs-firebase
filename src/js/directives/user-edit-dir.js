labs.directive('userEdit', [function() {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            user: '='
        },
        isolated: true,
        templateUrl : 'js/views/user-edit.html',
        controller: function ($scope) {
        	$scope.saveFirebase = function () {
				$scope.user.$save();
			};
        }
    };
}]);