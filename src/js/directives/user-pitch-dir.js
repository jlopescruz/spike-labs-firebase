labs.directive('userPitch', [function() {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            user: '='
        },
        isolated: true,
        templateUrl : 'js/views/user-pitch.html',
        controller: function ($scope) {
        	var WORD_LIMIT     = 500,
        	    FILE_LIMIT     = 5,
        	    aux_file_obj   = {};
    		
        	$scope.status = '1/3';
        	$scope.percentage = 33;

        	$scope.user.$loaded().then(function() {
        		$scope.checkCharacters();

        		$scope.filesRemaning = (typeof $scope.user.docs !== 'undefined')? FILE_LIMIT - $scope.user.docs.length : FILE_LIMIT ;
        	});

        	$scope.isSpinnerVisible = false;

        	$scope.countOf = function(text) {
			    var s = text ? text.split(/\s+/) : 0;
			    return s ? s.length : '';
			};

			$scope.checkCharacters = function () {
				$scope.wordsLeft = WORD_LIMIT - $scope.countOf($scope.user.pitch.description);
			};

			$scope.saveFirebase = function () {
				$scope.user.$save();
			};

            $scope.removeDoc = function (doc) { 
                $scope.user.docs.splice(doc,1);
                $scope.filesRemaning++;
                $scope.saveFirebase();
            };

			$scope.$on('s3upload:start', function(event, response, file) {
    			aux_file_obj.name = file.name;
    			aux_file_obj.lastModifiedDate = new Date(parseInt(file.lastModifiedDate, 10) * 1000).toLocaleString();
    			aux_file_obj.type = file.type;
    			$scope.isSpinnerVisible = true;
			});
	
			$scope.$on('s3upload:success', function(event, response, url) {
    			aux_file_obj.url = url.path;
    			
    			if (typeof $scope.user.docs === 'undefined') {
    				$scope.user.docs = [];
    				$scope.user.docs.push(aux_file_obj);
    			} else {
    				$scope.user.docs.push(aux_file_obj);
    			}

    			$scope.isSpinnerVisible = false;
    			$scope.saveFirebase();
    			$scope.filesRemaning--;
    			aux_file_obj = {};
			});
        }
    };
}]);