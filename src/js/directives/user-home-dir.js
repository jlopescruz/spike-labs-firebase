labs.directive('userHome', [function() {
    return {
        restrict: 'A',
        replace: false,
        scope: {
            user: '='
        },
        isolated: true,
        templateUrl : 'js/views/user-home.html',
        controller: function($scope, $rootScope) {

        	$scope.gotoTab = function (tabIndex) {
        		$rootScope.$emit('user:change-tab', tabIndex);
        	}
        }	
    };
}]);