module.exports = function (grunt) {

    'use strict';

    var SRC         = './src/';
    var DIST        = './dist/';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            all: [DIST]
        },

        copy: {
            options: {
                processContentExclude: ['.DS_Store', '.gitignore', '.sass-cache', 'node_modules']
            },
            fonts: {
                files: [
                    {
                        cwd: SRC,
                        dest: DIST,
                        src: ['fonts/**/*.{eot,otf,svg,ttf,woff}'],
                        expand: true,
                        filter: 'isFile'
                    }
                ]
            },
            images: {
                files: [
                    {
                        cwd: SRC,
                        dest: DIST,
                        src: ['images/*.{gif,jpg,png}'],
                        expand: true,
                        filter: 'isFile'
                    }
                ]
            },
            styles: {
                files: [
                    {
                        cwd: SRC,
                        dest: DIST,
                        src: ['css/**'],
                        expand: true,
                        filter: 'isFile'
                    }
                ]
            },
            html: {
                files: [
                    {
                        cwd: SRC,
                        dest: DIST,
                        src: ['*.html'],
                        expand: true,
                        filter: 'isFile'
                    },
                    {
                        cwd: SRC,
                        dest: DIST,
                        src: ['js/views/*.html'],
                        expand: true,
                        filter: 'isFile'
                    }
                ]  
            },
            php: {
                files: [
                    {
                        cwd: SRC,
                        dest: DIST,
                        src: ['php/**'],
                        expand: true,
                        filter: 'isFile'
                    }
                ]
            }
        },

        jshint: {
            options: {
                browser: true,
                curly: true,
                devel: true,
                eqeqeq: true,
                evil: true,
                immed: true,
                regexdash: true,
                asi : true,
                sub: true,
                trailing: true,
                globals: {
                    jQuery: true,
                    modernizr: true
                },
                force: true
            },
            dev: {
                src: [
                    SRC + 'js/*.js',
                    SRC + 'js/**/*.js',
                    '!' + SRC + 'js/libs/*.js'
                ]
            },
            gruntfile: {
                src: ['Gruntfile.js']
            }
        },

        sass: {
            options: {
                sourcemap: false,
                trace: true
            },
            dev: {
                options: {
                    style: 'expanded'
                },
                files: [{
                    expand: true,
                    src: ['**/*.scss', '!**/_*.scss'],
                    cwd: SRC + 'sass',
                    dest: SRC + 'css',
                    ext: '.css'
                }]
            },
            prod: {
                options: {
                    sourcemap: false,
                    style: 'compressed'
                },
                files: {
                    './src/css/main.min.css' : './src/sass/main.scss'
                }
            }
        },

        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd | hh:MM") %> */\n',
                mangle: false
            },
            build: {
                files: {
                    'dist/js/main.min.js': [
                        SRC + 'js/libs/angular.js',
                        SRC + 'js/libs/angular-sanitize.js',
                        SRC + 'js/libs/ng-s3upload.js',
                        SRC + 'js/libs/angular-route.js',
                        SRC + 'js/libs/angular-cookies.js',
                        SRC + 'js/libs/firebase.js',
                        SRC + 'js/libs/angularfire.js',
                        SRC + 'js/libs/ui-bootstrap.js',
                        SRC + 'js/app.js',
                        SRC + 'js/services/*.js',
                        SRC + 'js/controllers/*.js',
                        SRC + 'js/directives/*.js'
                    ]
                }
            }
        },

        watch: {
            options: {
                livereload: true
            },
            gruntfile: {
                expand: true,
                files: 'Gruntfile.js',
                tasks: ['clean', 'jshint', 'uglify', 'sass:prod', 'copy']
            },
            data: {
                expand:true,
                files: [SRC + 'data/*.json'],
                tasks: ['clean', 'jshint', 'uglify', 'sass:prod', 'copy']
            },
            sass: {
                expand: true,
                files: [SRC + '**/*.scss'],
                tasks: ['clean', 'jshint', 'uglify', 'sass:prod', 'copy']
            },
            scripts: {
                expand: true,
                files: [
                    SRC + 'js/libs/*.js',
                    SRC + 'js/views/*.html',
                    SRC + 'js/**/*.js',
                    SRC + 'js/*.js'
                ],
                tasks: ['clean', 'jshint', 'uglify', 'sass:prod', 'copy']
            },
            html: {
                expand: true,
                files: [SRC + '*.html'],
                tasks: ['clean', 'jshint', 'uglify', 'sass:prod', 'copy']
            }
        }

    });

    require('matchdep').filter('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.registerTask('run', ['watch']);
    grunt.registerTask('dev', ['clean', 'jshint', 'uglify', 'sass:prod', 'copy', 'watch']);
    grunt.registerTask('default', ['dev']);

};